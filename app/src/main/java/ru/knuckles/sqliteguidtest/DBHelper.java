package ru.knuckles.sqliteguidtest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(final Context context, final String name) {
        super(context, name, null, 1);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        init(db);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
    }

    private void init(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE int_ids (data CHARACTER);");

        db.execSQL(
                "CREATE TABLE random_ids (guid BLOB PRIMARY KEY DEFAULT (randomblob(16)), data CHARACTER) " +
                "WITHOUT ROWID;");

        db.execSQL("CREATE TABLE comb_ids (guid BLOB PRIMARY KEY, data CHARACTER) WITHOUT ROWID;");
        db.execSQL("CREATE VIEW  fake_view AS SELECT NULL as ts, NULL as data;");

        db.execSQL(
                "CREATE TRIGGER auto_guids INSTEAD OF INSERT ON fake_view BEGIN\n" +
                    "INSERT INTO comb_ids(guid, data) SELECT CAST(new_guid AS BLOB), NEW.data FROM (SELECT\n" +
                        // Convert partial timestamp to blob
                        "substr(b, (ts >> 40)       + 1, 1) ||\n" +
                        "substr(b, (ts >> 32) % 256 + 1, 1) ||\n" +
                        "substr(b, (ts >> 24) % 256 + 1, 1) ||\n" +
                        "substr(b, (ts >> 16) % 256 + 1, 1) ||\n" +
                        "substr(b, (ts >> 8)  % 256 + 1, 1) ||\n" +
                        "substr(b,  ts        % 256 + 1, 1) ||\n" +
                        // Add 10 random bytes
                        "randomblob(10) AS new_guid FROM (SELECT\n" +
                            "coalesce(\n" +
                                "NEW.ts,\n" +
                                "NEW.rowid + round((julianday('now') - 2440587.5) * 86400000)\n" +
                            ") & 0xFFFFFFFFFFFF as ts,\n" +  // Partial timestamp (6 lower bytes)
                            // Byte values list
                            "X'" +
                            "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F" +
                            "202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F" +
                            "404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F" +
                            "606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F" +
                            "808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9F" +
                            "A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF" +
                            "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF" +
                            "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF' as b" +
                        ")" +
                    ");" +
                "END;"
        );
    }
}
