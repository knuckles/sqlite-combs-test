package ru.knuckles.sqliteguidtest;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class PerfTest {
    private final static int INSERTS = 10 * 1000;
    private final static int INSERT_SERIES = 3;
    private final static int REPORT_INTERVAL = INSERTS / 5;
    private final static boolean INSERT_TRANSACTION = true;
    private final static boolean INTERMEDIATE_COMMITS = false;
    private final static String DB_NAME = "TestDB";

    @Before
    public void setUp() throws Exception {
        context = InstrumentationRegistry.getTargetContext();
        context.getDatabasePath(DB_NAME).delete();
        dbHelper = new DBHelper(context, DB_NAME);
        db = dbHelper.getWritableDatabase();
    }

    @After
    public void tearDown() throws Exception {
        if (db != null)
            db.close();
        if (dbHelper != null)
            dbHelper.close();
    }

    @Test
    public void testIntegerIds() throws Exception {
        final SQLiteStatement ins1 = db.compileStatement("INSERT INTO int_ids (data) VALUES (?);");
        runInserts(db, "int_ids", ins1, false);

        final SQLiteStatement sel1 = db.compileStatement("SELECT data FROM int_ids WHERE rowid = ?;");  // LIMIT 1 ?
        long beginTS = System.currentTimeMillis();
        long notFound = 0;
        Random r = new Random();
        db.beginTransaction();
        final int totalRecs = INSERTS * INSERT_SERIES;
        for (long i = 1; i <= totalRecs; i++) {
            sel1.bindLong(1, 1 + r.nextInt(totalRecs));
            try {
                sel1.simpleQueryForString();
            }
            catch (SQLiteDoneException e) {
                notFound++;
            }
        }
        db.endTransaction();
        long elapsed = System.currentTimeMillis() - beginTS;
        Log.i(getClass().getSimpleName(), String.format(Locale.ROOT,
                "Select time (int_ids): %dms", elapsed));
        assertEquals("Missing records in int_ids", 0, notFound);
    }

    @Test
    public void testRandomGUIDs() throws Exception {
        final String tableName = "random_ids";
        final SQLiteStatement ins2 = db.compileStatement("INSERT INTO random_ids (data) VALUES (?);");
        runInserts(db, tableName, ins2, false);
        runSelects(db, tableName);
    }

    @Test
    public void testOrderedGUIDs() throws Exception {
        final String tableName = "comb_ids";
        final SQLiteStatement ins3 = db.compileStatement("INSERT INTO fake_view(data, ts) VALUES (?, ?);");
        runInserts(db, tableName, ins3, true);
        runSelects(db, tableName);
    }

    private static void runInserts(final SQLiteDatabase db, final String tableName,
            final SQLiteStatement insertStmt, boolean withTS) throws InterruptedException {
        for (int i = 0; i < INSERT_SERIES; i++) {
            long ts = System.currentTimeMillis() + i * INSERTS;  // Separate series of timestamps.

            long elapsed = doInserts(db, insertStmt, withTS ? ts : 0);
            Log.i(PerfTest.class.getSimpleName(), String.format(Locale.ROOT,
                    "Total insert time for %d rows into %s: %dms", INSERTS, tableName, elapsed));
        }
    }

    private static long doInserts(final SQLiteDatabase db, final SQLiteStatement insertStmt, long startTs)
            throws InterruptedException {
        long beginTS = System.currentTimeMillis();
        if (INSERT_TRANSACTION)
            db.beginTransaction();
        try {
            _intermediateTimestamp = System.currentTimeMillis();
            for (int i = 0; i < INSERTS; i++) {
                insertStmt.bindString(1, "Test char data " + String.valueOf(i));
                if (startTs > 0)
                    insertStmt.bindLong(2, startTs + i);
                insertStmt.executeInsert();
                intermediate(db, i + 1);
            }
            if (INSERT_TRANSACTION)
                db.setTransactionSuccessful();
        }
        finally {
            if (INSERT_TRANSACTION)
                db.endTransaction();
        }
        return System.currentTimeMillis() - beginTS;
    }

    private static long _intermediateTimestamp = 0;
    private static void intermediate(final SQLiteDatabase db, final int inserted) {
        if (inserted <= 0 || inserted % REPORT_INTERVAL != 0)
            return;
        if (INSERT_TRANSACTION && INTERMEDIATE_COMMITS) {
            db.setTransactionSuccessful();
            db.endTransaction();
            db.beginTransaction();
        }
        final long now = System.currentTimeMillis();
        final long duration = now - _intermediateTimestamp;
        Log.i(PerfTest.class.getSimpleName(), String.format(Locale.ROOT,
                "Inserted %d rows; slice time: %dms", inserted, duration));
        _intermediateTimestamp = now;
    }

    private static void runSelects(final SQLiteDatabase db, final String tableName) {
        List<byte[]> guids = fetchGUIDs(db, tableName);
        Collections.shuffle(guids);

        db.beginTransaction();
        long elapsed = randomQueryByGUIDs(db, tableName, guids);
        db.endTransaction();
        Log.i(PerfTest.class.getSimpleName(), String.format(Locale.ROOT,
                "Random selection from %s took: %dms", tableName, elapsed));
    }

    private static List<byte[]> fetchGUIDs(SQLiteDatabase db, String tableName) {
        List<byte[]> result = new ArrayList<>();
        final Cursor c = db.query(tableName, new String[] {"guid"}, null, null, null, null, null);
        byte[] tsPart = new byte[] {0};
        int tsRepeatCounter = 0;
        ArrayList<Integer> tsRepeats = new ArrayList<>();
        try {
            final int idx = c.getColumnIndexOrThrow("guid");
            if (c.moveToFirst()) {
                do {
                    final byte[] bytes = c.getBlob(idx);

                    byte[] currTSPart = Arrays.copyOf(bytes, 6);
                    if (Arrays.equals(tsPart, currTSPart)) {
                        tsRepeatCounter++;
                    }
                    else if (tsRepeatCounter > 0) {
                        tsRepeats.add(tsRepeatCounter + 1);
                        tsRepeatCounter = 0;
                    }
                    tsPart = currTSPart;

                    result.add(bytes);
                } while (c.moveToNext());
            }
        }
        finally {
            c.close();
        }
        if (tsRepeats.size() > 0) {
            Collections.sort(tsRepeats);
            Log.i(PerfTest.class.getSimpleName(),
                    "Series of timestamps: " + String.valueOf(tsRepeats.size()) +
                    "; median length: " + String.valueOf(tsRepeats.get(tsRepeats.size() / 2)));
        }

        return result;
    }

    private static long randomQueryByGUIDs(SQLiteDatabase db, String tableName, List<byte[]> guids) {
        long notFound = 0;
        final SQLiteStatement s = db.compileStatement("SELECT data FROM " + tableName + " WHERE guid = ?;");
        long beginTS = System.currentTimeMillis();
        for (byte[] guid : guids) {
            s.bindBlob(1, guid);
            try {
                s.simpleQueryForString();
            }
            catch (SQLiteDoneException e) {
                notFound++;
            }
        }
        assertEquals("Missing records in " + tableName, 0, notFound);
        return System.currentTimeMillis() - beginTS;
    }

    private Context context;
    private DBHelper dbHelper = null;
    private SQLiteDatabase db = null;
}
